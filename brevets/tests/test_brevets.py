"""
Nose tests for acp_times.py
"""
import arrow
import nose
import logging
from brevets.acp_times import open_time, close_time

current = arrow.now()

def test_one():
    assert open_time(60, 200, current.isoformat()) == current.shift(hour = 1, minute = 46).isoformat()
    assert close_time(60, 200, current.isoformat()) == current.shift(hour = 4).isoformat()
    return

def test_two():
    assert open_time(120, 200, current.isoformat()) == current.shift(hour = 3, minute = 32).isoformat()
    assert close_time(120, 200, current.isoformat()) == current.shift(hour = 8).isoformat()
    return

def test_three():
    assert open_time(175, 200, current.isoformat()) == current.shift(hour = 5, minute = 9).isoformat()
    assert close_time(120, 200, current.isoformat()) == current.shift(hour = 11, minute = 40).isoformat()
    return

def test_four():
    assert open_time(200, 200, current.isoformat()) == current.shift(hour = 5, minute = 53).isoformat()
    return
