"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html
    """
    app.logger.debug("Got a JSON request")

    km = request.args.get('km', 999, type=float)
    total = request.args.get('distance', 1000, type=int)
    date = request.args.get('begin_date','2000-02-20')
    time = request.args.get('begin_time', '05:00')

    app.logger.debug("km={}".format(km))
    app.logger.debug("distance={}".format(total))
    app.logger.debug("date={}".format(date))
    app.logger.debug("time={}".format(time))
    app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(km, total, date + ' ' + time)
    close_time = acp_times.close_time(km, total, date + ' ' + time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(host= '0.0.0.0', port = CONFIG.PORT, debug = True)
