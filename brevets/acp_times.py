"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import re
#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

accepted_distances = [200, 300, 400, 600, 1000]
locations = [[0,200],[200,400],[400,600],[600,1000],[1000,1300]]
min_speed = [15, 15, 15, 11.428, 13.333]
max_speed = [34,32,30, 28,26]


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    if brevet_dist_km not in accepted_distances:
       return ValueError
    open = 0
    i = 0
    for loc in locations:
       if control_dist_km in range(loc[0], loc[1] + 1): #added one due to exclusive end of range
          open = (control_dist_km - loc[0]) / max_speed[i] #subtract the min of the range from the control distance
          i = i - 1 #decrement the index for the while loop below, as to not recount the same range/speed
          break
       i = i + 1
       while i >= 0:
          open = open + locations[i][1] / max_speed[i]
    hrs = open // 1
    min = (open - (open // 1))*60
    stamps = re.split('[- :.T]', brevet_start_time) #time stamps separated out from the ISO string
    new = arrow.now()
    newer = new.replace(year = int(stamps[0]), month = int(stamps[1]), day = int(stamps[2]), hour = int(stamps[3]), minute = int(stamps[4]))
    newest = newer.shift(hours=hrs, minutes= min)
    return newest.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    if brevet_dist_km not in accepted_distances:
       return ValueError
    open = 0
    i = 0
    for loc in locations:
       if control_dist_km in range(loc[0], loc[1] + 1): #added one due to exclusive end of range
          open = (control_dist_km - loc[0]) / min_speed[i] #subtract the min of the range from the control distance
          i = i - 1 #decrement the index for the while loop below, as to not recount the same range/speed
          break
       i = i + 1
       while i >= 0:
          open = open + locations[i][1] / min_speed[i]
    hrs = open // 1
    min = (open - (open // 1))*60    
    stamps = re.split('[- :.T]', brevet_start_time) #time stamps separated out from the ISO string
    new = arrow.now()
    newer = new.replace(year = int(stamps[0]), month = int(stamps[1]), day = int(stamps[2]), hour = int(stamps[3]), minute = int(stamps[4]))
    newest = newer.shift(hours=hrs, minutes=min)
    return newest.isoformat()
