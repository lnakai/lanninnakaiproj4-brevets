ACP calculator explanation:
Program for calculating brevet control open and close times.
These open and close times are calculated based off of what kilometer the rider will be on when going through that section. Look at https://rusa.org/pages/acp-brevet-control-times-calculator to view the increments these times will assigned through.
Some strange details of these calculations are that there should be relaxed control closing times when the location is less than 60 km away from the start. In general, try to avoid setting a control times within 20% of the total distance from the begining or the end of the brevet.
The closing time from 0 km is one hour.
There is a french version of the algorithm to deal with the difficulties of placing controls nearby the starting point of the brevet. The information is available in the above link but is not utilized here.
Our table is given its apperance by calc.html, then flask_brevets.py will take the arguments given through getJSON and push them through to acp_times.py, where the opening and closing times for the given control will be calculated, then AJAX will be used to display these results on the chart to the user.